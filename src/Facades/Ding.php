<?php

namespace DingDing\Laravel\Facades;

use Illuminate\Support\Facades\Facade;

class Ding extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'DingDing';
    }
}